package com.aset.ui.element;

import org.openqa.selenium.By;

class InputImpl extends ElementImpl implements Input {

    InputImpl(By by) {
        super(by);
    }

    @Override
    public String getText() {
        return webElement.getAttribute("value");
    }

    @Override
    public void setText(String text) {
        webElement.sendKeys(text);
    }
}
