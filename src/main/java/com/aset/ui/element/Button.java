package com.aset.ui.element;

public interface Button extends Element {

    boolean isClickable();

    void click();

}
