package com.aset.driver;

public enum WebDriverType {
    CHROME_WINDOWS,
    CHROME_LINUX,
    FIREFOX,
    IE,
    SAFARI,
    PHANTOM_JS_WINDOWS,
    PHANTOM_JS_LINUX,
}