package com.aset.ui.element;

public interface Input extends Element {

    String getText();

    void setText(String text);

}
