package com.aset.ui.element;

import org.openqa.selenium.By;

import java.util.List;

/**
 * API for work with UI element.
 *
 * @author a.darashkou
 */
public interface Element {

    Element getParent();

    Element findParent(By by);

    Element getChild();

    Element findChild(By by);

    List<Element> getChildren();

    List<Element> findChildren(By by);

    boolean isExists();

    boolean isEnabled();

    boolean isEditable();

    boolean isDisplayed();

    boolean isPresent();

    String getAttribute(String name);

}