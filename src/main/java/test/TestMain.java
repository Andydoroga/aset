package test;

import com.aset.driver.WebDriverContainer;
import com.aset.driver.WebDriverType;
import com.aset.ui.element.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class TestMain {

    private static final String HITBTC_URL = "https://hitbtc.com/";

    public static void main(String... args) {
        //TODO thinking about
        //webDriver.manage().timeouts().pageLoadTimeout(15, TimeUnit.MINUTES);

        WebDriverContainer.init(WebDriverType.CHROME_WINDOWS);

        WebDriver webDriver = WebDriverContainer.get();
        webDriver.get(HITBTC_URL);

        try {
            test(webDriver);
        } catch (NoSuchElementException e) {
            System.out.println("Such element doesn't exist: " + e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            WebDriverContainer.close();
        }
    }

    private static void test(WebDriver webDriver) throws NoSuchElementException, InterruptedException {
        //By by = By.xpath("//li[contains(@class,'menu-item')]//a[contains(@title, 'Алгоритмы')]");
        // By by2 = By.xpath("//div[contains(@class,'container')]");
        //Input input = ElementFactory.get(ElementType.INPUT, By.className("gsfi"));
        Button button = ElementFactory.get(ElementType.BUTTON, By.className("index_tickers__fullList"));
        button.click();


        Text table = ElementFactory.get(ElementType.TEXT, By.className("index_tickers__body"));
        table.getText();

//        if (webDriver instanceof JavascriptExecutor) {
//            ((JavascriptExecutor) webDriver).executeScript("var element = document.getElementsByClassName('index_tickers_column');");
//        }

        System.out.println(table.getText());
        //input.setText("Mother");

        //WebElement openSearchInput = webDriver.findElement(by);
        //((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", openSearchInput);

    }


}
