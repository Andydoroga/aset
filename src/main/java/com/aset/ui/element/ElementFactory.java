package com.aset.ui.element;

import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public final class ElementFactory {

    private static Map<ElementType, Function<By, ? extends Element>> map = new HashMap<>();
    static {
        map.put(ElementType.BUTTON, ButtonImpl::new);
        map.put(ElementType.INPUT, InputImpl::new);
        map.put(ElementType.SELECT, SelectImpl::new);
        map.put(ElementType.TEXT, TextImpl::new);
    }

    private ElementFactory() {
        throw new UnsupportedOperationException();
    }

    public static <T extends Element> T get(ElementType type, By by) {
        return (T) map.get(type).apply(by);
    }

}
