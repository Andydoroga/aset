package com.aset.exceptions;
/**
 * Page content not found errors wrapper
 */
public class PageContentNotFoundException extends RuntimeException {

    public PageContentNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}