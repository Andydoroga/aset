package com.aset.exceptions;
/**
 * ElementImpl not found errors wrapper
 */
public class ElementNotFoundException extends RuntimeException {

    public ElementNotFoundException(String errorMessage) {
        super(errorMessage);
    }

}