package com.aset.ui.element;

import org.openqa.selenium.By;

class ButtonImpl extends ElementImpl implements Button {

    ButtonImpl(By by) {
        super(by);
    }

    @Override
    public boolean isClickable() {
        return false;
    }

    @Override
    public void click() {
        webElement.click();
    }
}
