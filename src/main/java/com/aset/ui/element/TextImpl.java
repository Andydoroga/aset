package com.aset.ui.element;

import org.openqa.selenium.By;

class TextImpl extends ElementImpl implements Text {

    TextImpl(By by) {
        super(by);
    }

    @Override
    public String getText() {
        return webElement.getText();
    }
}
