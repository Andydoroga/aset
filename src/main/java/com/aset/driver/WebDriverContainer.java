package com.aset.driver;

import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

public final class WebDriverContainer {

    private static ThreadLocal<WebDriver> webDriver;
    private static final Set<WebDriver> driverPool = new HashSet<>();

    private WebDriverContainer() {
        throw new UnsupportedOperationException();
    }

    public synchronized static void init(final WebDriverType driverType) {
        webDriver = new ThreadLocal<WebDriver>() {
            @Override
            protected WebDriver initialValue() {
                WebDriver driver = WebDriverFactory.getDriver(driverType);
                driverPool.add(driver);

                return driver;
            }
        };
    }

    public synchronized static void close() {
        driverPool.forEach(WebDriver::close);
    }

    public synchronized static WebDriver get() {
        return webDriver.get();
    }

}