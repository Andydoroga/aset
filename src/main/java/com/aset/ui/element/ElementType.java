package com.aset.ui.element;

public enum ElementType {
    BUTTON,
    INPUT,
    SELECT,
    TABLE,
    TEXT
}