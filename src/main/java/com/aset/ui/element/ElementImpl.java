package com.aset.ui.element;

import com.aset.driver.WebDriverContainer;
import org.apache.commons.collections.CollectionUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

/**
 * API for work with UI element.
 */
class ElementImpl implements Element {

    final static WebDriver driver = WebDriverContainer.get();
    final WebElement webElement;

    ElementImpl(By by) {
        this.webElement = driver.findElement(by);
    }

    ElementImpl(WebElement webElement) {
        this.webElement = webElement;
    }

    @Override
    public Element getParent(){
        return new ElementImpl(webElement.findElement(By.xpath("..")));
    }

    @Override
    public Element findParent(By by) {
        // /ancestor::
        return new ElementImpl(webElement.findElement(By.xpath("..")));
    }

    //TODO отложенная инициализация мб?
    @Override
    public Element getChild(){
        List<Element> children = getChildren();
        return CollectionUtils.isNotEmpty(children) ? children.get(0) : null;
    }

    @Override
    public Element findChild(By by) {
        // /descendant::bar[1]
        return new ElementImpl(webElement.findElement(by));
    }

    @Override
    public List<Element> getChildren(){
        List<WebElement> children = webElement.findElements(By.xpath(".//*"));
        return children.stream().map(ElementImpl::new).collect(Collectors.toList());
    }

    @Override
    public List<Element> findChildren(By by){
        List<WebElement> children = webElement.findElements(by);
        return children.stream().map(ElementImpl::new).collect(Collectors.toList());
    }

    @Override
    public boolean isExists() {
        return false;
    }

    @Override
    public boolean isEnabled() {
        return webElement.isEnabled();
    }

    @Override
    public boolean isDisplayed(){
        return webElement.isDisplayed();
    }

    @Override
    public boolean isEditable(){
        return false;
    }

    @Override
    public boolean isPresent() {
        return false;
    }

    @Override
    public String getAttribute(String name) {
        return webElement.getAttribute(name);
    }

}