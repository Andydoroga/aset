package com.aset.ui.element;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

class SelectImpl extends ElementImpl implements Select {

    private int size;

    SelectImpl(By by) {
        super(by);
        size = getOptions().size();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public List<Option> getOptions() {
        List<WebElement> elements = webElement.findElements(By.tagName("option"));
        return elements.stream().map(OptionImpl::new).collect(Collectors.toList());
    }

    @Override
    public void select(Option option) {
        option.select();
    }

    @Override
    public void select(String optionName) {
        for (Option option : getOptions()) {
            if (optionName.equalsIgnoreCase(option.getText())) {
                option.select();
                return;
            }
        }
    }

    private static class OptionImpl implements Option {

        final WebElement webElement;

        OptionImpl(WebElement webElement) {
            this.webElement = webElement;
        }

        @Override
        public String getText() {
            return webElement.getText();
        }

        @Override
        public boolean isSelected() {
            return webElement.isSelected();
        }

        @Override
        public void select() {
            if (webElement.isDisplayed() && webElement.isEnabled()) {
                webElement.click();
            }
        }

    }
}
