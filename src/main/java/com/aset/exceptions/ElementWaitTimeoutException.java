package com.aset.exceptions;

/**
 * ElementImpl wait timeout errors wrapper
 */
public class ElementWaitTimeoutException extends RuntimeException {

    public ElementWaitTimeoutException(String errorMessage) {
        super(errorMessage);
    }

}