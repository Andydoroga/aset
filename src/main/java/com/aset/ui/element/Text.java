package com.aset.ui.element;

public interface Text extends Element {

    String getText();

}
