package com.aset.ui.element;

import java.util.List;

public interface Select extends Element {

    int size();

    List<Option> getOptions();

    void select(Option option);

    void select(String option);

    interface Option{
        String getText();
        boolean isSelected();
        void select();
    }

}
