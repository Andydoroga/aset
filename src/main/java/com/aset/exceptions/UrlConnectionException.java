package com.aset.exceptions;

public class UrlConnectionException extends RuntimeException {

    public UrlConnectionException(String message) {
        super(message);
    }

    public UrlConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

}