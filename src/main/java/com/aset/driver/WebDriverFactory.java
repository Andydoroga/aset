package com.aset.driver;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import java.io.File;

final class WebDriverFactory {

    private static final String CHROME_WINDOWS_DRIVER_PATH = "drivers\\chromedriver.exe";
    private static final String CHROME_LINUX_DRIVER_PATH = "drivers/chromedriver";
    private static final String IE_DRIVER_PATH = "drivers\\IEDriverServer.exe";
    private static final String PHANTOM_JS_WINDOWS_DRIVER_PATH = "drivers\\phantomjs_2.1.1-windows_x86_64.exe";
    private static final String PHANTOM_JS_LINUX_DRIVER_PATH = "drivers/phantomjs_2.1.1-linux-x86_64";

    private static final String CHROME_IGNORE_FLAG_CMD_ARGS = "test-type";
    private static final String CHROME_WORK_PROFILE = "--user-data-dir=";
    private static final String CHROME_DRIVER_BINARY_PROPERTY = "chrome.binary";
    private static final String CHROME_LINUX_BINARY_PATH = "/usr/bin/google-chrome-stable";

    private static final String USER_DIR_PROPERTY = "user.dir";

    private WebDriverFactory() {
        throw new UnsupportedOperationException();
    }

    static WebDriver getDriver(WebDriverType webDriverType) {
        switch (webDriverType) {
            case CHROME_WINDOWS:
                return getChromeDriver(CHROME_WINDOWS_DRIVER_PATH);
            case CHROME_LINUX:
                return getChromeDriver(CHROME_LINUX_DRIVER_PATH);
            case FIREFOX:
                return new FirefoxDriver();
            case IE:
                return getIEDriver(IE_DRIVER_PATH);
            case PHANTOM_JS_WINDOWS:
                return getPhantomJSDriver(PHANTOM_JS_WINDOWS_DRIVER_PATH);
            case PHANTOM_JS_LINUX:
                return getPhantomJSDriver(PHANTOM_JS_LINUX_DRIVER_PATH);
            case SAFARI:
                return new SafariDriver();
            default:
                return getChromeDriver(CHROME_WINDOWS_DRIVER_PATH);
        }
    }

    private static WebDriver getChromeDriver(String driverPath) {
        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, driverPath);

        ChromeOptions options = new ChromeOptions();
        options.addArguments(CHROME_IGNORE_FLAG_CMD_ARGS);

        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        if (CHROME_WINDOWS_DRIVER_PATH.equals(driverPath)) {
            capabilities.setCapability(CHROME_DRIVER_BINARY_PROPERTY, driverPath);
        } else {
            options.setBinary(CHROME_LINUX_BINARY_PATH);
            options.addArguments(CHROME_WORK_PROFILE + RandomStringUtils.randomAlphabetic(12));

        }
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        return new ChromeDriver(capabilities);
    }

    private static WebDriver getIEDriver(String driverPath) {
        System.setProperty(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY, driverPath);
        return new InternetExplorerDriver(InternetExplorerDriverService.createDefaultService());
    }

    private static WebDriver getPhantomJSDriver(String driverPath) {
        File phantomJsFile = new File(System.getProperty(USER_DIR_PROPERTY) + File.separator + driverPath);
        PhantomJSDriverService service = new PhantomJSDriverService.Builder().usingPhantomJSExecutable(phantomJsFile).usingAnyFreePort().build();
        return new PhantomJSDriver(service, new DesiredCapabilities());
    }

}